<?php
/**
 * @file
 * re_news.features.content.inc
 */

/**
 * Implements hook_content_defaults().
 */
function re_news_content_defaults() {
  $content = array();  

  $content['news_1'] = (object) array(
    'exported_path' => 'news/civic-election-reminder',
    'title' => 'Civic Election Reminder',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'news',
    'language' => 'en',
    'created' => '1329088421',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'news_1',
    'body' => array(
      'und' => array(
        0 => array(
          'value' => '<p><strong>For Immediate Release<br /></strong><strong>Our Town</strong></p><p>The Mayor\'s Office is reminding the public that the by-election for City Councillor will be held on Saturday, February 25<sup>th</sup>, 2012 from 8am-8pm at the Our Town Community Centre.</p><p>A shuttle service will be provided, every hour on the hour from 8am to 6pm, from the bus shelter on North Main street across from the Library. The return trip will depart from the Community Centre every half hour on the hour from 8:30am-6:30pm.&nbsp;</p><p>For more information, please contact:</p><p>Sarah Smith, Chief Election Officer</p><p>Phone: 456-789-0123</p><p><a href="mailto:abc@cinemas.com">elections@town.com</a></p>',
          'summary' => '',
          'format' => 'full_html',
          'safe_value' => '<p><strong>For Immediate Release<br /></strong><strong>Our Town</strong></p>
  <p>The Mayor\'s Office is reminding the public that the by-election for City Councillor will be held on Saturday, February 25<sup>th</sup>, 2012 from 8am-8pm at the Our Town Community Centre.</p>
  <p>A shuttle service will be provided, every hour on the hour from 8am to 6pm, from the bus shelter on North Main street across from the Library. The return trip will depart from the Community Centre every half hour on the hour from 8:30am-6:30pm.�</p>
  <p>For more information, please contact:</p>
  <p>Sarah Smith, Chief Election Officer</p>
  <p>Phone: 456-789-0123</p>
  <p><a href="mailto:abc@cinemas.com">elections@town.com</a></p>
  ',
          'safe_summary' => '',
        ),
      ),
    ),
  );

  $content['news_2'] = (object) array(
    'exported_path' => 'news/abc-cinemas-open-our-town',
    'title' => 'ABC Cinemas to Open in Our Town',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'news',
    'language' => 'en',
    'created' => '1325718786',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'news_2',
    'body' => array(
      'und' => array(
        0 => array(
          'value' => '<p><strong>For Immediate Release<br /></strong><strong>Our Town</strong></p><p>At last night\'s Town Council meeting the proposal by ABC Cinemas to build a new seven-screen 25,000 square feet movie complex in Our Town\'s fast-growing commercial district.&nbsp;</p><p>The new cinema will feature stadium seating, surround sound, two concession counters and larger seat\'s than the current cinema at the same location. There will also be a number of interactive games and activities on-site as well as an event room that can be booked for parties.</p><p>Construction is set to begin in mid-April and completed in time for the holiday season. XYZ, the current movie theatre, will be operating during construction.&nbsp;</p><p>For more information, please contact:&nbsp;</p><p>ABC Cinemas</p><p>Phone: 987-654-3210</p><p><a href="mailto:abc@cinemas.com">abc@cinemas.com</a></p>',
          'summary' => '',
          'format' => 'full_html',
          'safe_value' => '<p><strong>For Immediate Release<br /></strong><strong>Our Town</strong></p>
  <p>At last night\'s Town Council meeting the proposal by ABC Cinemas to build a new seven-screen 25,000 square feet movie complex in Our Town\'s fast-growing commercial district.�</p>
  <p>The new cinema will feature stadium seating, surround sound, two concession counters and larger seat\'s than the current cinema at the same location. There will also be a number of interactive games and activities on-site as well as an event room that can be booked for parties.</p>
  <p>Construction is set to begin in mid-April and completed in time for the holiday season. XYZ, the current movie theatre, will be operating during construction.�</p>
  <p>For more information, please contact:�</p>
  <p>ABC Cinemas</p>
  <p>Phone: 987-654-3210</p>
  <p><a href="mailto:abc@cinemas.com">abc@cinemas.com</a></p>
  ',
          'safe_summary' => '',
        ),
      ),
    ),
  );

  $content['news_3'] = (object) array(
    'exported_path' => 'news/local-government-partners-madd',
    'title' => 'Local Government Partners with MADD',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'news',
    'language' => 'en',
    'created' => '1322608347',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'news_3',
    'body' => array(
      'und' => array(
        0 => array(
          'value' => '<p><strong>For Immediate Release<br /></strong><strong>Our Town</strong></p><p>The Mayor\'s office announced today that it will be partnering MADD in an attempt to reduce drunk driving fatalities over the holiday season. Town Council has approved additional budget for increased policing during the month of December, including several checkpoints at major intersections in town as well as main roads leaving town.</p><p>Though the Mayor\'s office had been in discussions with MADD since late September, a deal was not reached until yesterday afternoon.</p><p>�Given the tragedies experienced in our community last year due to the effects of drunk driving my office is making it a priority to police and prevent any unnecessary fatalities this holiday season and into the New Year� remarked the Mayor when asked for comment.</p><p>The police blitz will begin on December 1<sup>st</sup>.&nbsp;</p><p>For more information, please contact:</p><p>City Hall</p><p>Main phonel ine: 123-456-7890 or</p><p><a href="mailto:mayor@office.com">mayor@office.com</a>.</p><p>&nbsp;</p><p>&nbsp;</p>',
          'summary' => '',
          'format' => 'full_html',
          'safe_value' => '<p><strong>For Immediate Release<br /></strong><strong>Our Town</strong></p>
  <p>The Mayor\'s office announced today that it will be partnering MADD in an attempt to reduce drunk driving fatalities over the holiday season. Town Council has approved additional budget for increased policing during the month of December, including several checkpoints at major intersections in town as well as main roads leaving town.</p>
  <p>Though the Mayor\'s office had been in discussions with MADD since late September, a deal was not reached until yesterday afternoon.</p>
  <p>�Given the tragedies experienced in our community last year due to the effects of drunk driving my office is making it a priority to police and prevent any unnecessary fatalities this holiday season and into the New Year� remarked the Mayor when asked for comment.</p>
  <p>The police blitz will begin on December 1<sup>st</sup>.�</p>
  <p>For more information, please contact:</p>
  <p>City Hall</p>
  <p>Main phonel ine: 123-456-7890 or</p>
  <p><a href="mailto:mayor@office.com">mayor@office.com</a>.</p>
  <p>�</p>
  <p>�</p>
  ',
          'safe_summary' => '',
        ),
      ),
    ),
  );
  
  return $content;
}
