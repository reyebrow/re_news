<?php
/**
 * @file
 * re_news.features.content_menu_links.inc
 */

/**
 * Implements hook_content_menu_links_defaults().
 */
function re_news_content_menu_links_defaults() {
  $menu_links = array();

  // Exported menu link: main-menu:news
  $menu_links['main-menu:news'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'news',
    'router_path' => 'news',
    'link_title' => 'News',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'node-name/mediaroom_page',
  );
  // Exported menu link: menu-main-menu-mobile-:news
  $menu_links['menu-main-menu-mobile-:news'] = array(
    'menu_name' => 'menu-main-menu-mobile-',
    'link_path' => 'news',
    'router_path' => 'news',
    'link_title' => 'News',
    'options' => array(
      'attributes' => array(
        'title' => 'News',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'node-name/mediaroom_page',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('News');


  return $menu_links;
}
