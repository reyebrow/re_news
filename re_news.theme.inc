<?php

/**
 * @file
 * Provides theming resources to be used in the News content type.
 */


/**
 * Implements of hook_theme().
 */
function re_news_theme($existing, $type, $theme, $path) {
  // Provide template for document content type:
  $document = isset($existing['node']) ? $existing['node'] : array();
  $document['path'] = $path . '/theme';
  $document['template'] = 'node--news';
  return array(
    'node__news' => $document,
  );
} // re_news_theme()